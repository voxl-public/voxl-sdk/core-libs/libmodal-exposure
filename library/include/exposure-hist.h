/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef EXPOSURE_HIST_H
#define EXPOSURE_HIST_H

#include <stdint.h>

struct modal_exposure_config_t
{
	// Defaults are OV7251, need per sensor configurations through config file
	uint16_t 		gain_min;
	uint16_t 		gain_max;
	uint32_t 		exposure_min_us;
	uint32_t 		exposure_max_us;

	// This is the main setpoint of the algorithm
	float 	 		desired_msv;
	// Gains
	float 			k_p_ns;
	float 			k_i_ns;
	// Maximum integral value
	float 			max_i;

	// hold-steady threshold
	unsigned int	p_good_thresh;

	unsigned int 	exposure_period;
	unsigned int 	gain_period;

	unsigned int 	display_debug;

	uint32_t		exposure_offset_for_gain_calc;
};

static void modal_exposure_print_config(modal_exposure_config_t c)
{
	printf("=================MODALAI Auto Exposure Settings==================\n");
	printf("gain_min:                        %d\n",    c.gain_min);
	printf("gain_max:                        %d\n",    c.gain_max);
	printf("exposure_min_us:                 %d\n",    c.exposure_min_us);
	printf("exposure_max_us:                 %d\n",    c.exposure_max_us);
	printf("desired_msv:                     %f\n",    c.desired_msv);
	printf("k_p_ns:                          %f\n",    c.k_p_ns);
	printf("k_i_ns:                          %f\n",    c.k_i_ns);
	printf("max_i:                           %f\n",    c.max_i);
	printf("p_good_thresh:                   %d\n",    c.p_good_thresh);
	printf("exposure_period:                 %d\n",    c.exposure_period);
	printf("gain_period:                     %d\n",    c.gain_period);
	printf("display_debug:                   %s\n",    (c.display_debug ? "yes" : "no"));
	printf("exposure_offset_for_gain_calc:   %d\n",    c.exposure_offset_for_gain_calc);
	printf("=================================================================\n");
}

class ModalExposureHist
{
public:
	ModalExposureHist(modal_exposure_config_t c)
	{
		ae_config = c;
		stored_gain = 0;

		// set ROI to values that will use the full image
		roi_x = 0;
		roi_y = 0;
		roi_width = 10000;
		roi_height = 10000;
	}

	bool update_exposure(
		uint8_t*	image_data, 
		int 		width,
		int 		height, 
		int64_t	    cur_exposure_ns,
		int32_t		cur_gain,
		int64_t* 	set_exposure_ns,
		int32_t* 	set_gain );

	void set_roi( 
		uint32_t x, 
		uint32_t y, 
		uint32_t width, 
		uint32_t height );

	

private:
	double compute_image_mean(	uint8_t* image_data, 
								uint16_t width, 
								uint16_t height);

	uint32_t 					counter = 0;
	bool 						first = true;
	double 						msv_prev = 0;
	float 						err_i;

	modal_exposure_config_t		ae_config;
	int16_t						stored_gain;
	uint32_t					roi_x;
	uint32_t					roi_y;
	uint32_t					roi_width;
	uint32_t					roi_height;


};

#endif // end #define CONFIG_FILE_H
