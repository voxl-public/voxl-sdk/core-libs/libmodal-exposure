/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/**
 *  @file exposure-hist.cpp
 *  @brief Main file for running the application
 */

// C/C++ Includes
#include <iostream>
#include <signal.h>
#include <cstring>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <math.h>
#include <float.h>

#define IIR_P 2.0

// Package dependencies
#include "exposure-hist.h"

#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif

#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif


/** Computes the mean of the image
 * 
 * @param image_data Array of image data (size Width x Height) where each char is a pixel value
 * @param width The width of the image
 * @param height The height of the image
 * @return The mean value of the image
 */
double ModalExposureHist::compute_image_mean(
	uint8_t*	image_data, 
	uint16_t 	width, 
	uint16_t 	height)
{
    // Compute the mean
    uint32_t number_of_pixels   = width * height;
    uint32_t accum_pixel_values = 0;
    double   mean_sample_value  = 0;

    //processing 4 pixels at a time is faster
    for(uint32_t i = 0; i < (number_of_pixels-3); i+=4)
    {
        accum_pixel_values += image_data[i];
        accum_pixel_values += image_data[i+1];
        accum_pixel_values += image_data[i+2];
        accum_pixel_values += image_data[i+3];
    }
    mean_sample_value = ((double)accum_pixel_values) / number_of_pixels;

    return mean_sample_value;
}


bool ModalExposureHist::update_exposure(
            uint8_t*    image_data,
            int         width,
            int         height,
            int64_t     cur_exposure_ns,
            int32_t     cur_gain,
            int64_t*    set_exposure_ns,
            int32_t*    set_gain )
{

	bool update 				= first; // set to true if values should be updated
	first 						= false;
	double mean_sample_value	= 0;
	
	mean_sample_value = compute_image_mean(image_data, width, height);

	// perform a simple iir filter on msv to try to squelch oscillations
	// IIR_P = 0 disables
	if( counter > IIR_P )
	{
		mean_sample_value = (mean_sample_value + (msv_prev*IIR_P)) / (IIR_P+1);
		msv_prev = mean_sample_value;
	}
	
	// PI controller around MSV
	float err_p = ae_config.desired_msv-mean_sample_value;
	err_i += err_p;
	if( fabs(err_i) > ae_config.max_i) 
	{
		err_i = (err_i>0?1:-1)*ae_config.max_i;
	}

	// Start with current values
	float set_exposure_us = cur_exposure_ns/1000;
	if( cur_gain == 0 )
	{
		// current gain could really be 0, but if so it will be computed 
		// properly in the gain calc.
		*set_gain = stored_gain;
	}
	else
	{
		*set_gain = cur_gain;
	}
	

	// Process exposure with PI controller
	// Don't change exposure if we're close enough.
	// this helps prevent oscillating
	if((counter%ae_config.exposure_period == 0) && 
		(fabs(err_p) > ae_config.p_good_thresh))
	{
		update = true;

		// Compute exposure based on PI errors
		set_exposure_us = 
			(cur_exposure_ns + ae_config.k_p_ns*err_p + ae_config.k_i_ns*err_i)/1000;

	}

	// Boundry check on exposure. First value from camera can be wrong
	modal_exposure_print_config(ae_config);
	printf("set us b4 check: %.3f\n", set_exposure_us);
	set_exposure_us = MIN( ae_config.exposure_max_us, set_exposure_us);
	set_exposure_us = MAX( ae_config.exposure_min_us, set_exposure_us);
	*set_exposure_ns = set_exposure_us * 1000;

	// if(ae_config.display_debug &&  (update || counter%1 == 0))
	// {
		printf( "set exposure[%d]: cur: %.3fus set: %.3fus %ldns msv: %.4f err_p: %.4f err_i: %.4f\n",
			counter, cur_exposure_ns/1000.f, set_exposure_us, *set_exposure_ns, mean_sample_value, 
			err_p, err_i);
	// }

	// Calculate gain is a scaler of exposure, with a fixed offset
	// the goal is for gain to move linearly with exposure 
	// but only above a certain exposure (the offset)
	// below the exposure offset, gain is 0
	// this is to avoid digital noise when it is not needed
	if( counter%ae_config.gain_period == 0 || *set_gain == 0 )
	{
		int32_t exposure_offset_range = 
			ae_config.exposure_max_us - ae_config.exposure_offset_for_gain_calc;
		float exposure_offset_div = exposure_offset_range / 
			(ae_config.gain_max - ae_config.gain_min);
		*set_gain = (set_exposure_us-ae_config.exposure_offset_for_gain_calc) / 
			exposure_offset_div;

		// save value for next frame in case real value isn't passed in
		stored_gain = *set_gain;
	}
	
	// Make sure gain doesn't go out of bounds
	*set_gain = MIN( ae_config.gain_max, *set_gain);
	*set_gain = MAX( ae_config.gain_min, *set_gain);

	counter++;

	return update;
}

void ModalExposureHist::set_roi( 
	uint32_t x, 
	uint32_t y, 
	uint32_t width, 
	uint32_t height )
{
	roi_x = x;
	roi_y = y;
	roi_width = width;
	roi_height = height;
}
