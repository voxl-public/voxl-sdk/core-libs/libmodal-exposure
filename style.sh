#!/bin/bash

STYLE_CHECKER_PY=cpplint.py
if test -f "$STYLE_CHECKER_PY"; then
    echo "$STYLE_CHECKER_PY exists."
else
    wget https://raw.githubusercontent.com/cpplint/cpplint/develop/cpplint.py
fi

python $STYLE_CHECKER_PY library/src/*

exit 0

